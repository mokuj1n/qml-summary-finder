import os
import sys


BASE_DIR = os.path.dirname(os.path.abspath(__name__))
DOCX_DIR = os.path.join(BASE_DIR, 'files')


# Temp files
CACHE_FILE_PREFIX = '.cache.picle'
CACHE_FILE_PATH = os.path.join(BASE_DIR, CACHE_FILE_PREFIX)


# Application info
APPLICATION_VERSION = '0.2.1'